import sys
from pymongo import UpdateOne
global bulk=db.
# Function to update record to mongo db
def update(db,id,loclist,mag):
	try:

		db.quakeRT.update_one(
			{"id":id },
			{
				"$set": {
					"locations":loclist,
					"magnitudes":mag
				}
			}
		)
		print("DONE");

	except:
		print('Unexpected error: ',sys.exc_info() ) ;
