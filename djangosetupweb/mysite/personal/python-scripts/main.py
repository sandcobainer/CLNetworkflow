import urllib.request
from bs4 import BeautifulSoup
from getdata_cl import *
import string


if __name__ == '__main__':
	invalid_tags = ['b', 'i', 'u', 'br']
	places=[]
	for i in listrides():
		with urllib.request.urlopen(i['url']) as response:
			html = response.read()
			soup = BeautifulSoup(html, 'html.parser')
			#read and build soup
			ride_status = soup.find('p', {'class': 'attrgroup'}).span.string
			area=soup.body.section.header.li.a.text
			content = soup.find('section', {'id': 'postingbody'}, True)
			tags = content.find(not_inside_toc)
			content = str(content).replace(str(tags), '')
			con = BeautifulSoup(content, 'html.parser')
			# get ride stat,area,content
			for tag in invalid_tags:
				for match in con.findAll(tag):
					match.replaceWithChildren()
					#print(match)
			text = con.text
			#clean content by removing html tags
			print(text)
			translator = str.maketrans('', '', string.punctuation)

			getplaces(text)
			stplaces(text)
			geotextplaces(text)

			places.append(area)
			#run nltk to find place names
			#computelatlong(places,ride_status)
			#print('nltk plain places :' ,places)





