from pymongo import MongoClient
import re
from geotext import GeoText
from nltk.tag import StanfordNERTagger
from nltk.tokenize import word_tokenize
import string
import sys


st = StanfordNERTagger('/Users/sandeepdasari/Downloads/stanford-ner-2014-06-16/classifiers/english.muc.7class.distsim.crf.ser.gz',
					   '/Users/sandeepdasari/Downloads/stanford-ner-2014-06-16/stanford-ner.jar',
					    encoding='utf-8')

client=MongoClient('mongodb://test:password@volare.kdd.cs.ksu.edu:7017/new') #connection
translator = str.maketrans('', '', string.punctuation)
db=client.new #new db
quake=db.cafires# quakes collection
c=0;
count=0;
bulk = quake.initialize_unordered_bulk_op()

for tweet in quake.find().limit(10):
	text=tweet['text']
	# print(text)

	loclist=[];orglist=[];datelist=[];timelist=[];
	editext=text
	#print('***Cleaned text***')
	preprotext=re.sub("(@[A-Za-z0-9]+)|(?<!\d)[().,#\-;:](?!\d)|(\w+:\/\/\S+)", " ", editext) #|([^0-9A-Za-z \t])
	#preprotext = re.sub("([/.!$%^&*()#])|((^[0-9\.0-9]))", "", preprotext)
	#print(preprotext)

	tokenized_text = word_tokenize(preprotext)
	classified_text = st.tag(tokenized_text)
	places=GeoText(preprotext)
	for k in places.cities:
		loclist.append(k)
	for k in places.countries:
		loclist.append(k)
	for i in classified_text:
		if(i[1]=='LOCATION'):
			if i[0] not in loclist:
				loclist.append(i[0])
		elif (i[1] == 'ORGANIZATION'):
			if i[0] not in orglist:
				orglist.append(i[0])
		elif (i[1] == 'DATE'):
			if i[0] not in datelist:
				datelist.append(i[0])
		elif (i[1] == 'TIME'):
			if i[0] not in timelist:
				timelist.append(i[0])

	print(orglist)
	print(datelist)
	#regex = re.compile(r"([0-9]*\.[0-9]*)")
	regex = re.compile(r"([-+]?\d*\.\d+|\d+)")
	mag=regex.findall(preprotext)
	print(mag,loclist)
	if len(loclist)>0 and len(mag)>0:
		print(preprotext) #do some update in the db
		id=tweet['id']
		try:
			bulk.find(
				{"id": id}).update({
						"$set": {
							"locations": loclist,
							#"magnitudes": mag,
							"organizations": orglist,
							"dates":datelist,
							"times":timelist
						}
					})
			print("DONE");
			count=count+1;
			if (count % 1000 == 0):
				bulk.execute()
				bulk = quake.initialize_unordered_bulk_op()

		except:
			print('Unexpected error: ', sys.exc_info())

	print('-----------',c,'--------------')
	c=c+1;
if (count % 1000 != 0):
	bulk.execute()