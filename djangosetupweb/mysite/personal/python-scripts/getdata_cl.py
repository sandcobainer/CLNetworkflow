from craigslist import CraigslistJobs, CraigslistForSale, CraigslistCommunity, CraigslistEvents
import nltk, nltk.tag, nltk.chunk
from geotext import GeoText
from nltk.tag import StanfordNERTagger

from geotext import GeoText
from nltk.tokenize import word_tokenize

def listrides(): #list rides from ksu website
	cl_c = CraigslistCommunity(site='ksu', area='', category='rid',filters={})
	i=0
	posts=[]
	for result in cl_c.get_results(sort_by='newest', geotagged=True):
	    i=i+1
	    posts.append(result)
	return posts
	#cl_e = CraigslistEvents(site='ksu', filters={'free': True, 'food': True})

def not_inside_toc(tag):  #except tag
    return tag.get('class') != ['print-information print-qrcode-container'] or tag.clear()

def extract_entity_names(t):
    entity_names = []
    if hasattr(t, 'label') and t.label:
        if t.label() == 'NE':
            entity_names.append(' '.join([child[0] for child in t]))
        else:
            for child in t:
                entity_names.extend(extract_entity_names(child))
    return entity_names

def getplaces(text):  #nltk get places
	#convert text into sentences
	sentences = nltk.sent_tokenize(text)
	# Tokenize the sentences - ie. split them into words
	tokenized_sents = [nltk.word_tokenize(sentence) for sentence in sentences]
	# Tag these words with Part-of-Speech tags (noun, verb, etc)
	tagged_sents = [nltk.pos_tag(sentence) for sentence in tokenized_sents]
	# Create named entity chunks from these tagged words

	chunked_sents = nltk.ne_chunk_sents(tagged_sents, binary=True)
	# Extract a list of named entities (people, places, etc)
	entities = []
	for tree in chunked_sents:
		print(tree)
		#nltk.chunk.conllstr2tree(tree, chunk_types=['NP']).draw()
		entities.extend(extract_entity_names(tree))
	#return entities

def stplaces(text):
	loclist=[]
	orglist=[]
	datelist=[]
	timelist=[]
	st = StanfordNERTagger(
		'/Users/sandeepdasari/Downloads/stanford-ner-2014-06-16/classifiers/english.muc.7class.distsim.crf.ser.gz',
		'/Users/sandeepdasari/Downloads/stanford-ner-2014-06-16/stanford-ner.jar',
		encoding='utf-8')
	tokenized_text = word_tokenize(text)
	classified_text = st.tag(tokenized_text)
	for i in classified_text:
		if(i[1]=='LOCATION'):
			if i[0] not in loclist:
				loclist.append(i[0])
		elif (i[1] == 'ORGANIZATION'):
			if i[0] not in orglist:
				orglist.append(i[0])
		elif (i[1] == 'DATE'):
			if i[0] not in datelist:
				datelist.append(i[0])
		elif (i[1] == 'TIME'):
			if i[0] not in timelist:
				timelist.append(i[0])
	print('ST place list :',loclist)


def geotextplaces(text):
	loclist=[]
	places = GeoText(text)
	for k in places.cities:
		loclist.append(k)
	for k in places.countries:
		loclist.append(k)
	print('Geotext placelist :',loclist)

